package com.simplexdcc.main.tab

import com.simplexdcc.AddToQueueSheet
import com.simplexdcc.main.MainWindow
import com.simplexdcc.service.TransferManagementService
import org.apache.pivot.collections.Sequence
import org.apache.pivot.wtk.Button
import org.apache.pivot.wtk.ButtonPressListener
import org.apache.pivot.wtk.PushButton
import org.apache.pivot.wtk.Span
import org.apache.pivot.wtk.TableView
import org.apache.pivot.wtk.TableViewSelectionListener

/**
 * User: Vincent
 * Date: 1/13/13
 * Time: 2:00 PM
 */
class QueueTab {
    def namespace
    MainWindow mainWindow

    def init() {
        def queueTable = namespace.get("queueTable")
        def addToQueueButton = namespace.get("addToQueueButton")
        def moveUpButton = namespace.get("moveUpButton")
        def moveDownButton = namespace.get("moveDownButton")
        def removeButton = namespace.get("removeButton")

        queueTable.selectMode = TableView.SelectMode.MULTI

        moveUpButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button button) {
                def selectedIndex = queueTable.selectedRanges[0].start
                def items = queueTable.tableData.remove selectedIndex, 1
                queueTable.tableData.insert items[0], selectedIndex - 1
            }
        })

        moveDownButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button button) {
                def selectedIndex = queueTable.selectedRanges[0].start
                def items = queueTable.tableData.remove selectedIndex, 1
                queueTable.tableData.insert items[0], selectedIndex + 1
            }
        })

        removeButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button button) {
                def selectedRanges = queueTable.selectedRanges
                while(selectedRanges.length != 0) {
                    queueTable.tableData.remove(selectedRanges[0].start, selectedRanges[0].length as int)
                }
            }
        })

        queueTable.tableViewSelectionListeners.add(new TableViewSelectionListener.Adapter() {
            @Override
            void selectedRangesChanged(TableView tableView, Sequence<Span> previousSelectedRanges) {
                def selectedRanges = tableView.selectedRanges
                if (selectedRanges.length == 0) {
                    moveUpButton.enabled = false
                    moveDownButton.enabled = false
                    removeButton.enabled = false
                } else if (selectedRanges.length == 1) {
                    if (selectedRanges[0].start == selectedRanges[0].end) {
                        moveUpButton.enabled = selectedRanges[0].start != 0
                        moveDownButton.enabled = selectedRanges[0].end != tableView.tableData.length - 1
                        removeButton.enabled = true
                    } else {
                        moveUpButton.enabled = false
                        moveDownButton.enabled = false
                        removeButton.enabled = true
                    }
                } else {
                    moveUpButton.enabled = false
                    moveDownButton.enabled = false
                    removeButton.enabled = true
                }
            }
        })

        addToQueueButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button button) {
                def alreadyExists = {tableData, row ->
                    for(rowItem in tableData) {
                        if(rowItem == row) {
                            return true
                        }
                    }
                    return false
                }
                def addToQueue = {String botNick, String saveTo, int from, int to, int interval ->
                    for(int i = from; i <= to; i+=interval) {
                        def tableData = queueTable.tableData
                        def row = [botNick: botNick, packNumber: i, saveTo: saveTo]
                        if (!alreadyExists(tableData, row)) {
                            queueTable.tableData.add row
                        }
                    }
                }
                def addToQueueSheet = new AddToQueueSheet()
                addToQueueSheet.open mainWindow, addToQueue
            }
        })
    }
}
