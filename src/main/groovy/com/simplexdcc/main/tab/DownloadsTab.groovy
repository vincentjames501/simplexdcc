package com.simplexdcc.main.tab

import com.simplexdcc.XDCCManagerBot
import com.simplexdcc.component.TableViewMeterCellRenderer
import com.simplexdcc.main.MainWindow
import com.simplexdcc.service.TransferManagementService
import org.apache.pivot.wtk.Button
import org.apache.pivot.wtk.ButtonPressListener
import org.apache.pivot.wtk.MessageType
import org.apache.pivot.wtk.Prompt
import org.apache.pivot.wtk.PushButton
import org.apache.pivot.wtk.TableView
import org.apache.pivot.wtk.TableViewSelectionListener

import java.awt.Desktop
import java.awt.EventQueue

/**
 * User: Vincent
 * Date: 1/13/13
 * Time: 5:29 PM
 */
class DownloadsTab {
    def namespace
    MainWindow mainWindow
    static downloadsTableData
    static downloadsTable

    def init() {
        downloadsTable = namespace.get("downloadsTable")
        def openFileButton = namespace.get("openFileButton")
        def openDirectoryButton = namespace.get("openDirectoryButton")
        def removeSelectedButton = namespace.get("removeSelectedButton")
        def removeAllCompleted = namespace.get("removeAllCompleted")
        def retrySelectedButton = namespace.get("retrySelectedButton")
        def stopSelectedButton = namespace.get("stopSelectedButton")

        downloadsTable.columns[3].cellRenderer = new TableViewMeterCellRenderer()

        downloadsTableData = downloadsTable.tableData

        stopSelectedButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button button) {
                def selectedRanges = downloadsTable.selectedRanges
                def rowData = downloadsTable.tableData[selectedRanges[0].start]
                TransferManagementService.instance.stopTransfer rowData.botNick, rowData.packNumber
            }
        })

        retrySelectedButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button button) {
                def selectedRanges = downloadsTable.selectedRanges
                def rowData = downloadsTable.tableData[selectedRanges[0].start]
                XDCCManagerBot.instance.sendMessage rowData.botNick, "xdcc send #" + rowData.packNumber
            }
        })

        removeAllCompleted.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button button) {
                def getRemovableIndex = {
                    downloadsTable.tableData.findIndexOf {
                        it.status == "Completed"
                    }
                }
                def removeIndex = getRemovableIndex()
                while (removeIndex != -1) {
                    downloadsTable.tableData.remove removeIndex, 1
                    removeIndex = getRemovableIndex()
                }
            }
        })

        removeSelectedButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button button) {
                def selectedRanges = downloadsTable.selectedRanges
                downloadsTable.tableData.remove selectedRanges[0].start, selectedRanges[0].length
            }
        })

        openFileButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button button) {
                def selectedIndex = downloadsTable.selectedRanges[0].start
                if (Desktop.isDesktopSupported()) {
                    def desktop = Desktop.getDesktop();
                    try {
                        desktop.open(new File((String)downloadsTable.tableData[selectedIndex].saveTo + (String)downloadsTable.tableData[selectedIndex].file));
                    } catch (Exception e) {
                        e.printStackTrace()
                        Prompt.prompt(MessageType.ERROR, "Failed to open file", mainWindow)
                    }
                }
            }
        })

        openDirectoryButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button button) {
                def selectedIndex = downloadsTable.selectedRanges[0].start
                if (Desktop.isDesktopSupported()) {
                    def desktop = Desktop.getDesktop();
                    try {
                        desktop.open(new File(downloadsTable.tableData[selectedIndex].saveTo as String));
                    } catch (Exception e) {
                        e.printStackTrace()
                        Prompt.prompt(MessageType.ERROR, "Failed to open directory", mainWindow)
                    }
                }
            }
        })

        downloadsTable.tableViewSelectionListeners.add(new TableViewSelectionListener.Adapter() {
            @Override
            void selectedRowChanged(TableView tableView, Object previousSelectedRow) {
                def selectedRanges = tableView.selectedRanges
                openFileButton.enabled = false
                openDirectoryButton.enabled = false
                removeSelectedButton.enabled = false
                retrySelectedButton.enabled = false
                stopSelectedButton.enabled = false

                if (selectedRanges.length == 1) {
                    openDirectoryButton.enabled = true
                    def rowData = tableView.tableData[selectedRanges[0].start]
                    switch (rowData.status) {
                        case "Completed":
                            openFileButton.enabled = true
                            removeSelectedButton.enabled = true
                            break;
                        case "Stopped":
                            retrySelectedButton.enabled = true
                            break;
                        case "In Progress":
                            stopSelectedButton.enabled = true
                            break;
                    }
                }
            }
        })
    }

    static getExistingRow(botNick, packNumber) {
        for (row in downloadsTableData) {
            if (row.botNick == botNick && row.packNumber == packNumber) {
                return row
            }
        }
    }

    static addTransferRow(botNick, packNumber, saveTo) {
        def rowData = getExistingRow botNick, packNumber
        if (rowData != null) {
            return rowData
        }
        rowData = [botNick: botNick, packNumber: packNumber, file: "", progress: "0.00 %", percentage: "0%", speed: "0 KB/s", status: "Queued", saveTo: saveTo]
        EventQueue.invokeAndWait(new Runnable() {
            @Override
            void run() {
                downloadsTableData.add rowData
                downloadsTable.repaint true
            }
        })
        return rowData
    }

    static refresh() {
        EventQueue.invokeAndWait new Runnable() {
            @Override
            void run() {
                downloadsTable.repaint true
            }
        }
    }
}
