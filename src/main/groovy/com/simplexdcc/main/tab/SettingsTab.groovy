package com.simplexdcc.main.tab

import com.simplexdcc.main.MainWindow
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import org.apache.pivot.util.Filter
import org.apache.pivot.wtk.Alert
import org.apache.pivot.wtk.BoxPane
import org.apache.pivot.wtk.Button
import org.apache.pivot.wtk.ButtonPressListener
import org.apache.pivot.wtk.FileBrowserSheet
import org.apache.pivot.wtk.Form
import org.apache.pivot.wtk.MessageType
import org.apache.pivot.wtk.PushButton
import org.apache.pivot.wtk.Sheet
import org.apache.pivot.wtk.SheetCloseListener
import org.apache.pivot.wtk.TextInput
import org.apache.pivot.wtk.validation.IntRangeValidator

/**
 * User: Vincent
 * Date: 1/25/13
 * Time: 6:08 PM
 */
class SettingsTab {
    def namespace
    MainWindow mainWindow
    def static settings = [maxDownloads: 4, maxQueue: 2, defaultDownloadDirectory: " "]

    def init() {
        def maxDownloadsPane = namespace.get("maxDownloadsPane")
        def maxDownloadsInput = namespace.get("maxDownloadsInput") as TextInput
        def maxQueuedPane = namespace.get("maxQueuedPane")
        def maxQueueInput = namespace.get("maxQueueInput") as TextInput
        def defaultDownloadDirectoryPane = namespace.get("defaultDownloadDirectoryPane")
        def defaultDownloadDirectoryInput = namespace.get("defaultDownloadDirectoryInput") as TextInput
        def errorLabel = namespace.get("errorLabel")
        def saveButton = namespace.get("saveButton") as PushButton
        def browseButton = namespace.get("browseButton") as PushButton

        def settingsFile = new File(".settings")
        if (settingsFile.exists()) {
            settings = new JsonSlurper().parseText(settingsFile.text)
            settings.maxDownloads = settings.maxDownloads.toInteger()
            settings.maxQueue = settings.maxQueue.toInteger()
            settings.defaultDownloadDirectory = settings.defaultDownloadDirectory.trim()
        } else {
            settingsFile.createNewFile()
            settingsFile.write new JsonBuilder(settings).toPrettyString()
        }
        maxDownloadsInput.text = settings.maxDownloads
        maxQueueInput.text = settings.maxQueue
        defaultDownloadDirectoryInput.text = settings.defaultDownloadDirectory

        maxQueueInput.validator = new IntRangeValidator(0, 100)
        maxDownloadsInput.validator = new IntRangeValidator(0, 100)

        saveButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button button) {
                def maxDownloads = maxDownloadsInput.text
                def maxQueue = maxQueueInput.text
                def downloadsDirectory = defaultDownloadDirectoryInput.text

                Form.clearFlag maxDownloadsPane
                Form.clearFlag maxQueuedPane
                Form.clearFlag defaultDownloadDirectoryPane

                errorLabel.text = null
                def wasError = false

                if (maxDownloads.length() == 0 || !maxDownloadsInput.textValid) {
                    Form.setFlag(maxDownloadsPane, new Form.Flag(MessageType.ERROR, "Max download input is required."));
                    wasError = true
                }
                if (maxQueue.length() == 0 || !maxQueueInput.textValid) {
                    Form.setFlag(maxQueuedPane, new Form.Flag(MessageType.ERROR, "Max queue input is required."));
                    wasError = true
                }
                if (wasError) {
                    errorLabel.setText("Some required information is missing.");
                } else {
                    settings = [maxDownloads: maxDownloads.toInteger(), maxQueue: maxQueue.toInteger(), defaultDownloadDirectory: downloadsDirectory]
                    settingsFile.delete()
                    settingsFile.createNewFile()
                    settingsFile.write(new JsonBuilder(settings).toPrettyString())
                    Alert.alert(MessageType.INFO, "Successfully saved settings", mainWindow)
                    errorLabel.text = null
                }
            }
        })

        browseButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button b) {
                def fileBrowserSheet = new FileBrowserSheet()
                fileBrowserSheet.setMode FileBrowserSheet.Mode.SAVE_AS
                def input = fileBrowserSheet.getContainerListeners().iterator().next().getAt("saveAsTextInput") as TextInput
                def saveAsPane = fileBrowserSheet.getContainerListeners().iterator().next().getAt("saveAsBoxPane") as BoxPane
                input.text = " "
                saveAsPane.visible = false
                fileBrowserSheet.disabledFileFilter = new Filter<File>() {
                    @Override
                    boolean include(File item) {
                        return item.isFile()
                    }
                }
                fileBrowserSheet.open(mainWindow, new SheetCloseListener() {
                    @Override
                    public void sheetClosed(Sheet fileBrowsingSheet) {
                        if (fileBrowsingSheet.getResult()) {
                            def selectedFiles = fileBrowserSheet.getSelectedFiles()
                            if (selectedFiles.length == 1) {
                                defaultDownloadDirectoryInput.text = selectedFiles[0].getAbsolutePath().trim()
                            }
                        }
                    }
                });
            }
        })
    }
}
