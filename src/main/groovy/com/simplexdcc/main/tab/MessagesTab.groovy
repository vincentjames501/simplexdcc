package com.simplexdcc.main.tab

import com.simplexdcc.XDCCManagerBot
import com.simplexdcc.main.MainWindow
import org.apache.pivot.wtk.Button
import org.apache.pivot.wtk.ButtonPressListener
import org.apache.pivot.wtk.PushButton
import org.apache.pivot.wtk.TableView
import org.apache.pivot.wtk.TextArea
import org.apache.pivot.wtk.TextInput

/**
 * User: Vincent
 * Date: 1/13/13
 * Time: 2:11 PM
 */
class MessagesTab {
    def namespace
    def mainWindow

    def init() {
        def messagesArea = namespace.get("messagesArea")
        def messageInput = namespace.get("messageInput")
        def sendMessageButton = namespace.get("sendMessageButton")
        def xdccManagerBot = XDCCManagerBot.instance
        xdccManagerBot.stopRecordingUnreadMessages messagesArea
        sendMessageButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button button) {
                xdccManagerBot.sendRawLine messageInput.text
            }
        })
    }
}
