package com.simplexdcc.main

import com.simplexdcc.component.TableViewMeterCellRenderer
import com.simplexdcc.main.tab.DownloadsTab
import com.simplexdcc.main.tab.MessagesTab
import com.simplexdcc.main.tab.QueueTab
import com.simplexdcc.main.tab.SettingsTab
import com.simplexdcc.service.TransferManagementService
import org.apache.pivot.beans.Bindable
import org.apache.pivot.collections.Map
import org.apache.pivot.collections.Sequence
import org.apache.pivot.util.Resources
import org.apache.pivot.wtk.Button
import org.apache.pivot.wtk.ButtonPressListener
import org.apache.pivot.wtk.MessageType
import org.apache.pivot.wtk.Prompt
import org.apache.pivot.wtk.PushButton
import org.apache.pivot.wtk.Span
import org.apache.pivot.wtk.TableView
import org.apache.pivot.wtk.TableViewSelectionListener
import org.apache.pivot.wtk.Window

import java.awt.Desktop
import java.awt.Graphics2D;

/**
 * User: Vincent
 * Date: 1/12/13
 * Time: 7:32 PM
 */
class MainWindow extends Window implements Bindable {
    def initFileTransferService(namespace) {
        setIcon(getClass().classLoader.getResource("icons/main_icon.png"))
        def queueTable = namespace.get("queueTable")
        def downloadsTable = namespace.get("downloadsTable")
        TransferManagementService.instance.start(queueTable.tableData, downloadsTable.tableData)
    }

    void initialize(Map<String, Object> namespace, URL location, Resources resources) {
        new MessagesTab(namespace: namespace, mainWindow: this).init()
        new QueueTab(namespace: namespace, mainWindow: this).init()
        new DownloadsTab(namespace: namespace, mainWindow: this).init()
        new SettingsTab(namespace: namespace, mainWindow: this).init()
        initFileTransferService namespace
    }
}