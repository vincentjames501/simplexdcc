package com.simplexdcc

import com.simplexdcc.service.TransferManagementService
import org.jibble.pircbot.DccFileTransfer
import org.jibble.pircbot.PircBot

import java.awt.EventQueue

/**
 * User: Vincent
 * Date: 1/12/13
 * Time: 5:20 PM
 */
@Singleton
class XDCCManagerBot extends PircBot {
    def registered = false
    def stopLogging = false
    def ircConnector
    def messagesArea
    def unreadMessages = ""

    synchronized void log(String line) {
        println line
        if (!stopLogging) {
            synchronized (this) {
                unreadMessages += line + "\n"
            }
        } else {
            addToMessagesTextArea line
        }
    }

    void onMessage(String channel, String sender,
                          String login, String hostname, String message) {
        println channel + " : " + sender + " : " + login + " : " + hostname + " : " + message
    }

    void onUserMode(String targetNick, String sourceNick, String sourceLogin, String sourceHostname, String mode) {
        if (mode.equalsIgnoreCase("+r") && targetNick.equalsIgnoreCase(getNick())) {
            registered = true
            ircConnector?.registeredSuccessfully()
        }
    }

    void onIncomingFileTransfer(DccFileTransfer fileTransfer) {
        Thread.start {
            TransferManagementService.instance.handleIncomingFileTransfer(fileTransfer)
        }
    }

    void onFileTransferFinished(DccFileTransfer transfer, Exception e) {
        Thread.start {
            TransferManagementService.instance.fileTransferFinished(transfer, e)
        }
    }

    def updateBotNick(nick) {
        name = nick
    }

    def isRegistered() {
        return registered;
    }

    def addToMessagesTextArea(line) {
        EventQueue.invokeAndWait(new Runnable() {
            @Override
            void run() {
                messagesArea.text = messagesArea.text + line + "\n"
                messagesArea.selectionStart = messagesArea.characterCount
            }
        })
    }

    def stopRecordingUnreadMessages(messagesArea) {
        this.messagesArea = messagesArea
        stopLogging = true
        synchronized (this) {
            messagesArea.text = messagesArea.text + unreadMessages + "\n"
            messagesArea.selectionStart = messagesArea.characterCount
        }
    }
}
