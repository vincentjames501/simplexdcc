package com.simplexdcc

import com.simplexdcc.main.MainWindow
import com.simplexdcc.main.tab.SettingsTab
import org.apache.pivot.beans.BXMLSerializer
import org.apache.pivot.util.Filter
import org.apache.pivot.wtk.BindType
import org.apache.pivot.wtk.BoxPane
import org.apache.pivot.wtk.Button
import org.apache.pivot.wtk.ButtonPressListener
import org.apache.pivot.wtk.FileBrowserSheet
import org.apache.pivot.wtk.Form
import org.apache.pivot.wtk.Label
import org.apache.pivot.wtk.ListButton
import org.apache.pivot.wtk.ListView
import org.apache.pivot.wtk.MessageType
import org.apache.pivot.wtk.PushButton
import org.apache.pivot.wtk.Sheet
import org.apache.pivot.wtk.SheetCloseListener
import org.apache.pivot.wtk.TextInput
import org.apache.pivot.wtk.TextInputBindingListener
import org.apache.pivot.wtk.TextInputListener
import org.apache.pivot.wtk.TextInputSelectionListener
import org.apache.pivot.wtk.validation.Validator

/**
 * User: Vincent
 * Date: 1/13/13
 * Time: 10:38 AM
 */
class AddToQueueSheet {
    def botNicks

    def open(MainWindow window, Closure closure) {
        def serializer = new BXMLSerializer()
        def sheet = serializer.readObject(getClass().getClassLoader().getResourceAsStream("layouts/add_to_queue_sheet.bxml")) as Sheet
        sheet.open window

        def addButton = serializer.getNamespace().get("addButton") as PushButton

        def errorLabel = serializer.getNamespace().get("errorLabel") as Label

        def xdccBotNickFilterInput = serializer.getNamespace().get("xdccBotNickFilterInput") as TextInput
        def xdccBotNickListView = serializer.getNamespace().get("xdccBotNick") as ListView
        def saveToFolderInput = serializer.getNamespace().get("saveToFolderInput") as TextInput
        def browseButton = serializer.getNamespace().get("browseButton") as PushButton
        def fromInput = serializer.getNamespace().get("fromInput") as TextInput
        def toInput = serializer.getNamespace().get("toInput") as TextInput
        def intervalInput = serializer.getNamespace().get("intervalInput") as TextInput

        def saveToFolderPane = serializer.getNamespace().get("saveToFolderPane") as BoxPane
        def xdccBotNickPane = serializer.getNamespace().get("xdccBotNickPane") as BoxPane
        def fromPane = serializer.getNamespace().get("fromPane") as BoxPane
        def toPane = serializer.getNamespace().get("toPane") as BoxPane
        def intervalPane = serializer.getNamespace().get("intervalPane") as BoxPane

        saveToFolderInput.text = SettingsTab.settings.defaultDownloadDirectory

        def convertToStringArray = {users ->
            def userNames = []
            users.each {
                if (it.nick[0] == '%' || it.nick[0] == '&') {
                    userNames.add it.nick[1..-1]
                } else {
                    userNames.add it.nick
                }
            }
            userNames
        }
        def userNames = convertToStringArray XDCCManagerBot.instance.getUsers(XDCCManagerBot.instance.channels[0])
        userNames.sort()
        userNames.each {
            xdccBotNickListView.listData.add it
        }
        botNicks = userNames.clone()

        xdccBotNickFilterInput.textInputSelectionListeners.add(new TextInputSelectionListener() {
            @Override
            void selectionChanged(TextInput textInput, int previousSelectionStart, int previousSelectionLength) {
                def filterQuery = textInput.text
                def filteredList = botNicks.findAll {
                    it.toLowerCase().contains filterQuery.toLowerCase()
                }
                xdccBotNickListView.listData.clear()
                filteredList.sort()
                filteredList.each {
                    xdccBotNickListView.listData.add it
                }
            }
        })

        browseButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button b) {
                def fileBrowserSheet = new FileBrowserSheet()
                fileBrowserSheet.setMode FileBrowserSheet.Mode.SAVE_AS
                def input = fileBrowserSheet.getContainerListeners().iterator().next().getAt("saveAsTextInput") as TextInput
                def saveAsPane = fileBrowserSheet.getContainerListeners().iterator().next().getAt("saveAsBoxPane") as BoxPane
                input.text = " "
                saveAsPane.visible = false
                fileBrowserSheet.disabledFileFilter = new Filter<File>() {
                    @Override
                    boolean include(File item) {
                        return item.isFile()
                    }
                }
                fileBrowserSheet.open(window, new SheetCloseListener() {
                    @Override
                    public void sheetClosed(Sheet fileBrowsingSheet) {
                        if (fileBrowsingSheet.getResult()) {
                            def selectedFiles = fileBrowserSheet.getSelectedFiles()
                            if (selectedFiles.length == 1) {
                                saveToFolderInput.setText selectedFiles[0].getAbsolutePath()
                            }
                        }
                    }
                });
            }
        })

        addButton.buttonPressListeners.add(new ButtonPressListener() {
            @Override
            void buttonPressed(Button b) {
                def xdccBoxNick = xdccBotNickListView.getSelectedItem() as String;
                def saveToFolder = saveToFolderInput.getText();
                def from = fromInput.getText();
                def to = toInput.getText();
                def interval = intervalInput.getText();
                def wasError = false

                Form.clearFlag xdccBotNickPane
                Form.clearFlag saveToFolderPane
                Form.clearFlag fromPane
                Form.clearFlag toPane
                Form.clearFlag intervalPane

                if (xdccBoxNick == null || xdccBoxNick.length() == 0) {
                    Form.setFlag(xdccBotNickPane, new Form.Flag(MessageType.ERROR, "Bot nick is required."))
                    wasError = true
                }
                if (saveToFolder.length() == 0) {
                    Form.setFlag(saveToFolderPane, new Form.Flag(MessageType.ERROR, "Directory is required."))
                    wasError = true
                }
                if (from.length() == 0 || !from.isInteger()) {
                    Form.setFlag(fromPane, new Form.Flag(MessageType.ERROR, "From input is required."))
                    wasError = true
                }
                if (to.length() == 0 || !to.isInteger()) {
                    Form.setFlag(toPane, new Form.Flag(MessageType.ERROR, "To input is required."))
                    wasError = true
                }
                if (interval.length() == 0 || !interval.isInteger()) {
                    Form.setFlag(intervalPane, new Form.Flag(MessageType.ERROR, "Interval input is required."))
                    wasError = true
                }
                if (wasError) {
                    errorLabel.setText "Some required information is missing."
                    return
                }

                to = to.toInteger()
                from = from.toInteger()
                interval = interval.toInteger()
                if (to < from) {
                    errorLabel.setText "From input must be less than to input"
                    return
                }
                if (interval - 1 > (to-from)) {
                    errorLabel.setText "Interval should not be greater than input range"
                    return
                }

                errorLabel.setText null
                sheet.close()
                closure xdccBoxNick, saveToFolder, from, to, interval
            }
        })
    }
}
