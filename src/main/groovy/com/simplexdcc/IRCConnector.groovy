package com.simplexdcc

/**
 * User: Vincent
 * Date: 1/12/13
 * Time: 5:26 PM
 */
class IRCConnector {
    def userName
    def channel
    def nickservPass
    def server
    def loginWindow
    def connectThread

    def connect() {
        def bot = XDCCManagerBot.instance
        bot.ircConnector = this
        connectThread = Thread.start {
            bot.updateBotNick userName
            bot.setVerbose true
            try {
                bot.connect server
                if (!nickservPass.isEmpty()) {
                    if (!registerNick(bot)) {
                        return
                    }
                }
            } catch (Exception e) {
                loginWindow.failedToConnect(e.getMessage())
                return
            }
            channel = channel[0] != "#" ? "#${channel}" : channel
            bot.joinChannel channel
            loginWindow.successfullyConnected()
        }
    }

    synchronized boolean registerNick(bot) {
        bot.identify nickservPass
        wait 10000
        if (!bot.isRegistered()) {
            loginWindow.failedToConnect("Failed to register nick properly and in a timely fashion!")
            return false
        }
        return true
    }

    synchronized registeredSuccessfully() {
        notify()
    }
}
