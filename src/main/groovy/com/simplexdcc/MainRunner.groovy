package com.simplexdcc

import org.apache.pivot.beans.BXMLSerializer
import org.apache.pivot.collections.Map
import org.apache.pivot.wtk.Application
import org.apache.pivot.wtk.DesktopApplicationContext
import org.apache.pivot.wtk.Display
import org.apache.pivot.wtk.Window

/**
 * User: Vincent
 * Date: 1/12/13
 * Time: 1:59 PM
 */
class MainRunner implements Application {
    static Display display

    public static void main(String[] args) {
        DesktopApplicationContext.main(MainRunner.class, args);
    }

    @Override
    public void startup(Display display, Map<String, String> properties)
    throws Exception {
        this.display = display
        def window = (Window)new BXMLSerializer().readObject(getClass().getClassLoader().getResourceAsStream("layouts/login_window.bxml"))
        window.open(display)
    }

    @Override
    public boolean shutdown(boolean optional) {
        return false
    }

    @Override
    public void suspend() {
    }

    @Override
    public void resume() {
    }
}

