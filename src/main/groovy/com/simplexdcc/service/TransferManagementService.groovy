package com.simplexdcc.service

import com.simplexdcc.XDCCManagerBot
import com.simplexdcc.main.tab.DownloadsTab
import com.simplexdcc.main.tab.SettingsTab
import org.jibble.pircbot.DccFileTransfer

import java.awt.EventQueue

/**
 * User: Vincent
 * Date: 1/14/13
 * Time: 8:16 PM
 */
@Singleton
class TransferManagementService {
    def queuedPacksTableData
    def downloadsTableData
    def activeTransfers = []
    def queuedPacks = []

    def monitorFileTransfer = {DccFileTransfer fileTransfer, rowData ->
        fileTransfer.receive new File(rowData.saveTo + System.getProperty("file.separator") + fileTransfer.file.name), true
        activeTransfers.add([fileTransfer: fileTransfer, rowData: rowData])
        rowData.status = "In Progress"
        rowData.file = fileTransfer.file.name
        while(fileTransfer.open) {
            rowData.progress = fileTransfer.progressPercentage / 100
            rowData.percentage = fileTransfer.progressPercentage.round(2).toString() + "%"
            rowData.speed = (fileTransfer.transferRate / 1024).toInteger().toString() + " KB/s"
            DownloadsTab.refresh()
            sleep 1000
        }
        if (fileTransfer.progressPercentage.toInteger() == 100) {
            rowData.status = "Completed"
        }
    }

    def fileTransferFinished(fileTransfer, e) {
        def foundTransfer = activeTransfers.find {
            it.fileTransfer == fileTransfer
        }
        activeTransfers.remove(foundTransfer)
        foundTransfer.rowData.speed = ""
        if (e != null) {
            e.printStackTrace()
            foundTransfer.rowData.status = "Stopped"
        } else {
            foundTransfer.rowData.progress = 1
            foundTransfer.rowData.percentage = "100%"
            foundTransfer.rowData.status = "Completed"
        }
    }

    def startMonitorFileTransfer(rowData, botNick, packNumber) {
        Thread.start {
            queuedPacks.add([botNick: botNick, packNumber: packNumber, rowData: rowData, callback: monitorFileTransfer])
            XDCCManagerBot.instance.sendMessage botNick, "xdcc send #" + packNumber
        }
    }

    def handleIncomingFileTransfer(DccFileTransfer fileTransfer) {
        println "Incoming file transfer from ${fileTransfer.nick} for file ${fileTransfer.file.name}"
        def existingDownload = downloadsTableData.find {
            it.file == fileTransfer.file.name
        }
        if (existingDownload) {
            monitorFileTransfer(fileTransfer, existingDownload)
            return
        }
        def queuedPack = queuedPacks.find {
            it.botNick.equalsIgnoreCase(fileTransfer.nick)
        }
        if (queuedPack) {
            queuedPacks.remove queuedPack
            queuedPack.callback fileTransfer, queuedPack.rowData
        } else {
            println "Couldn't find row in downloads table for ${fileTransfer.nick} for file ${fileTransfer.file.name}"
        }
    }

    def getQueuedCount(botNick) {
        def count = 0
        for(downloadRow in downloadsTableData) {
            if (downloadRow.botNick == botNick && downloadRow.status == "Queued") {
                count++
            }
        }
        count
    }

    def findPackToRemove() {
        for(queuedPack in queuedPacksTableData) {
            if (getQueuedCount(queuedPack.botNick) < SettingsTab.settings.maxQueue.toInteger()) {
                return queuedPack
            }
        }
    }

    def start(queueData, downloadsData) {
        queuedPacksTableData = queueData as org.apache.pivot.collections.List<?>
        downloadsTableData = downloadsData
        Thread.start {
            while(true) {
                if (!queuedPacksTableData.isEmpty() && activeTransfers.size() < SettingsTab.settings.maxDownloads.toInteger()) {
                    def toQueue
                    EventQueue.invokeAndWait(new Runnable() {
                        @Override
                        void run() {
                            toQueue = findPackToRemove()
                            if (toQueue) {
                                queuedPacksTableData.remove queuedPacksTableData.indexOf(toQueue), 1
                            }
                        }
                    })
                    if (toQueue) {
                        def row = DownloadsTab.addTransferRow toQueue.botNick, toQueue.packNumber, toQueue.saveTo
                        startMonitorFileTransfer(row, toQueue.botNick, toQueue.packNumber)
                    }
                }
                sleep 1000
            }
        }
    }

    def stopTransfer(botNick, packNumber) {
        activeTransfers.each {item->
            if(item.rowData.botNick == botNick && item.rowData.packNumber == packNumber) {
                def fileTransfer = item.fileTransfer as DccFileTransfer
                fileTransfer.close()
                Thread.start {
                    sleep 1100
                    item.rowData.status = "Stopped"
                    DownloadsTab.refresh()
                }
            }
        }
    }
}
