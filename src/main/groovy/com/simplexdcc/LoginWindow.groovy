package com.simplexdcc

import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import org.apache.pivot.beans.BXMLSerializer
import org.apache.pivot.beans.Bindable;
import org.apache.pivot.collections.Map;
import org.apache.pivot.util.Resources;
import org.apache.pivot.wtk.Button;
import org.apache.pivot.wtk.ButtonPressListener;
import org.apache.pivot.wtk.BoxPane
import org.apache.pivot.wtk.Checkbox;
import org.apache.pivot.wtk.Form;
import org.apache.pivot.wtk.Label;
import org.apache.pivot.wtk.MessageType;
import org.apache.pivot.wtk.Prompt;
import org.apache.pivot.wtk.PushButton;
import org.apache.pivot.wtk.TextInput;
import org.apache.pivot.wtk.Window

import java.awt.EventQueue;

/**
 * User: Vincent
 * Date: 1/12/13
 * Time: 2:23 PM
 */
class LoginWindow extends Window implements Bindable {
    def connectingPrompt

    @Override
    public void initialize(Map<String, Object> namespace, URL location, Resources resources) {
        setIcon(getClass().classLoader.getResource("icons/main_icon.png"))
        def usernameBoxPane = namespace.get("usernameBoxPane") as BoxPane
        def channelBoxPane = namespace.get("channelBoxPane") as BoxPane
        def serverHostBoxPane = namespace.get("serverHostBoxPane") as BoxPane

        def userNameInput = namespace.get("userNameInput") as TextInput
        def nickServPassInput = namespace.get("nickServPassInput") as TextInput
        def channelInput = namespace.get("channelInput") as TextInput
        def serverHostInput = namespace.get("serverHostInput") as TextInput
        def saveCheckBox = namespace.get("saveCheckBox") as Checkbox

        def submitButton = namespace.get("submitButton") as PushButton
        def errorLabel = namespace.get("errorLabel") as Label

        def savedFile = new File(".simplexdcc")
        if (savedFile.exists()) {
            def savedDetails = new JsonSlurper().parseText savedFile.getText()
            userNameInput.setText savedDetails.userName
            nickServPassInput.setText savedDetails.nickservPass
            channelInput.setText savedDetails.channel
            serverHostInput.setText savedDetails.serverHost
            saveCheckBox.setSelected true
            savedFile.delete()
        }

        submitButton.getButtonPressListeners().add(new ButtonPressListener() {
            @Override
            public void buttonPressed(Button button) {
                def userName = userNameInput.getText();
                def channel = channelInput.getText();
                def serverHost = serverHostInput.getText();
                def nickservPass = nickServPassInput.getText();
                def wasError = false

                Form.clearFlag usernameBoxPane
                Form.clearFlag channelBoxPane
                Form.clearFlag serverHostBoxPane

                if (userName.length() == 0) {
                    Form.setFlag(usernameBoxPane, new Form.Flag(MessageType.ERROR, "Username is required."));
                    wasError = true
                }
                if (channel.length() == 0) {
                    Form.setFlag(channelBoxPane, new Form.Flag(MessageType.ERROR, "Channel is required."));
                    wasError = true
                }
                if (serverHost.length() == 0) {
                    Form.setFlag(serverHostBoxPane, new Form.Flag(MessageType.ERROR, "Server is required."));
                    wasError = true
                }

                if (wasError) {
                    errorLabel.setText("Some required information is missing.");
                    return
                }
                if (saveCheckBox.selected) {
                    def savedJsonDetails = new JsonBuilder([userName: userName, nickservPass: nickservPass, serverHost: serverHost, channel: channel]).toPrettyString()
                    savedFile.write(savedJsonDetails)
                }
                errorLabel.setText(null);
                connectingPrompt = new Prompt(MessageType.INFO, "Connecting...", null, null);
                connectingPrompt.open(LoginWindow.this, null);
                new IRCConnector(userName: userName, nickservPass: nickservPass, server: serverHost, loginWindow: this, channel: channel).connect()
            }
        })
    }

    def successfullyConnected() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            void run() {
                connectingPrompt?.close()
                close()
                def window = (Window)new BXMLSerializer().readObject(getClass().getClassLoader().getResourceAsStream("layouts/main_window.bxml"));
                window.open(MainRunner.display);
            }
        })
    }

    def failedToConnect(String errorMessage) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            void run() {
                println errorMessage
                XDCCManagerBot.instance.disconnect()
                connectingPrompt?.close()
                Prompt.prompt(MessageType.ERROR, errorMessage, LoginWindow.this)
            }
        })
    }
}