package com.simplexdcc.component;

import org.apache.pivot.wtk.Meter;
import org.apache.pivot.wtk.Orientation;
import org.apache.pivot.wtk.TableView;
import org.apache.pivot.wtk.TableView.CellRenderer;

/**
 * User: Vincent
 * Date: 1/13/13
 * Time: 2:42 PM
 */
class TableViewMeterCellRenderer implements CellRenderer {
    @Delegate Meter meter = new Meter(Orientation.HORIZONTAL)

    @Override
    public void render(Object row, int rowIndex, int columnIndex,
                       TableView tableView, String columnName,
                       boolean selected, boolean highlighted, boolean disabled) {
        if (row != null) {
            setPercentage row.percentage.replace("%", "").toDouble() / 100
        }
    }

    @Override
    public String toString(Object row, String columnName) {
        return null;
    }


}
